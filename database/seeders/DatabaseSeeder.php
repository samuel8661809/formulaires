<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Formulaire;
use App\Models\Option;
use App\Models\Question;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $user = User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        $user2 = User::factory()->create([
            'name' => 'John Doe',
            'email' => 'john@gmail.com',
        ]);

        $formulaire = Formulaire::create([
            'user_id' => $user->id,
            'form_name' => 'Some Random Questions'
        ]);

        $question = Question::create([
            'formulaire_id' => $formulaire->id,
            'type' => 'text',
            'placeholder' => null,
            'placeholder_en' => null,
            'label' => 'Prenom',
            'label_en' => 'First Name',
            'required' => true,
            'default_value' => 'Valeur',
            'default_value_en' => 'Value',
            'min_date' => null,
            'max_date' => null,
            'half' => false,
            'class_name' => null,
            'helper' => null,
            'helper_en' => null,
            'title' => null,
            'title_en' => null,
            'min_value' => null,
            'max_value' => null,
            'show_checkbox' => false,
            'show_periodique' => false,
        ]);

        $formulaire2 = Formulaire::create([
            'user_id' => $user2->id,
            'form_name' => 'Personality'
        ]);

        Question::create([
            'formulaire_id' => $formulaire2->id,
            'type' => 'text',
            'placeholder' => null,
            'placeholder_en' => null,
            'label' => 'Nom de famille',
            'label_en' => 'Last Name',
            'required' => true,
            'default_value' => 'Valeur',
            'default_value_en' => 'Value',
            'min_date' => null,
            'max_date' => null,
            'half' => false,
            'class_name' => null,
            'helper' => null,
            'helper_en' => null,
            'title' => null,
            'title_en' => null,
            'min_value' => null,
            'max_value' => null,
            'show_checkbox' => false,
            'show_periodique' => false,
        ]);

        Question::create([
            'formulaire_id' => $formulaire2->id,
            'type' => 'text',
            'placeholder' => null,
            'placeholder_en' => null,
            'label' => 'Fruit',
            'label_en' => 'Fruit',
            'required' => true,
            'default_value' => 'Valeur',
            'default_value_en' => 'Value',
            'min_date' => null,
            'max_date' => null,
            'half' => false,
            'class_name' => null,
            'helper' => null,
            'helper_en' => null,
            'title' => null,
            'title_en' => null,
            'min_value' => null,
            'max_value' => null,
            'show_checkbox' => false,
            'show_periodique' => false,
        ]);

        Option::create([
            'question_id' => $question->id,
            'option_value' => 'Voiture',
            'option_value_en' => 'Car',
        ]);
        Option::create([
            'question_id' => $question->id,
            'option_value' => 'Autobus',
            'option_value_en' => 'Bus',
        ]);
        Option::create([
            'question_id' => $question->id,
            'option_value' => 'Train',
            'option_value_en' => 'Train',
        ]);
    }
}
