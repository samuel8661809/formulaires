<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('formulaire_id')->constrained()->onDelete('cascade');
            $table->integer('field_number')->nullable();
            $table->string('type');
            $table->string('placeholder')->nullable();
            $table->string('placeholder_en')->nullable();
            $table->string('label')->nullable();
            $table->string('label_en')->nullable();
            $table->boolean('required')->default(false);
            $table->string('default_value')->nullable();
            $table->string('default_value_en')->nullable();
            $table->date('min_date')->nullable();
            $table->date('max_date')->nullable();
            $table->boolean('half')->default(false);
            $table->string('class_name')->nullable();
            $table->longText('helper')->nullable();
            $table->longText('helper_en')->nullable();
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();
            $table->integer('min_value')->nullable();
            $table->integer('max_value')->nullable();
            $table->boolean('show_checkbox')->default(false);
            $table->boolean('show_periodique')->default(false);
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('questions');
    }
};
