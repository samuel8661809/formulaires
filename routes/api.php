<?php

use App\Http\Controllers\FormulaireController;
use App\Http\Controllers\OptionController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/formulaires', [FormulaireController::class, 'index']);

Route::post('/formulaires', [FormulaireController::class, 'store'])->middleware('auth:sanctum');

Route::put('/formulaires/{formulaire}', [FormulaireController::class, 'update'])->middleware('auth:sanctum');

Route::delete('/formulaires/{formulaire}', [FormulaireController::class, 'destroy'])->middleware('auth:sanctum');

Route::get('/formulaires/{formulaire}', [FormulaireController::class, 'show']);



Route::get('/formulaires/{formulaire}/questions', [QuestionController::class, 'index']);

Route::get('/formulaires/{formulaire}/questions/{question}', [QuestionController::class, 'show']);

Route::post('/formulaires/{formulaire}/questions', [QuestionController::class, 'store'])->middleware('auth:sanctum');

Route::delete('/formulaires/{formulaire}/questions/{question}', [QuestionController::class, 'destroy'])->middleware('auth:sanctum');

Route::put('/formulaires/{formulaire}/questions/{question}', [QuestionController::class, 'update'])->middleware('auth:sanctum');



Route::get('/formulaires/{formulaire}/questions/{question}/options', [OptionController::class, 'index']);

Route::get('/formulaires/{formulaire}/questions/{question}/options/{option}', [OptionController::class, 'show']);

Route::post('/formulaires/{formulaire}/questions/{question}/options', [OptionController::class, 'store'])->middleware('auth:sanctum');

Route::delete('/formulaires/{formulaire}/questions/{question}/options/{option}', [OptionController::class, 'destroy'])->middleware('auth:sanctum');

Route::put('/formulaires/{formulaire}/questions/{question}/options/{option}', [OptionController::class, 'update'])->middleware('auth:sanctum');



Route::post('/register', [UserController::class, 'register']);

Route::post('/login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->post('/logout', [UserController::class, 'logout']);
