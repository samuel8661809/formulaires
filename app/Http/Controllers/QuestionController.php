<?php

namespace App\Http\Controllers;

use App\Models\Formulaire;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    // Show all Formulaire Questions
    public function index(Formulaire $formulaire) {
        $questions = $formulaire->questions()->get();
        return response()->json($questions);
    }

    // Show One Formulaire Question
    public function show(Formulaire $formulaire, Question $question)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }
        return response()->json($question);
    }

    // Store Question
    public function store(Request $request, Formulaire $formulaire)
    {
        $formFields = $request->validate([
            'field_number' => 'nullable|numeric',
            'type' => 'required|string',
            'placeholder' => 'nullable|string',
            'placeholder_en' => 'nullable|string',
            'label' => 'nullable|string',
            'label_en' => 'nullable|string',
            'required' => 'nullable|boolean',
            'default_value' => 'nullable|string',
            'default_value_en' => 'nullable|string',
            'min_date' => 'nullable|date',
            'max_date' => 'nullable|date',
            'half' => 'nullable|boolean',
            'class_name' => 'nullable|string',
            'helper' => 'nullable|string',
            'helper_en' => 'nullable|string',
            'title' => 'nullable|string',
            'title_en' => 'nullable|string',
            'min_value' => 'nullable|numeric',
            'max_value' => 'nullable|numeric',
            'show_checkbox' => 'nullable|boolean',
            'show_periodique' => 'nullable|boolean',
        ]);

        $question = $formulaire->questions()->create($formFields);

        return response()->json($question, 201);
    }

    // Delete Question
    public function destroy(Formulaire $formulaire, Question $question)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        $question->delete();

        return response()->json(['message' => 'Question deleted successfully']);
    }

    // Update Question
    public function update(Request $request, Formulaire $formulaire, Question $question)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        $formFields = $request->validate([
            'field_number' => 'nullable|numeric',
            'type' => 'required|string',
            'placeholder' => 'nullable|string',
            'placeholder_en' => 'nullable|string',
            'label' => 'nullable|string',
            'label_en' => 'nullable|string',
            'required' => 'nullable|boolean',
            'default_value' => 'nullable|string',
            'default_value_en' => 'nullable|string',
            'min_date' => 'nullable|date',
            'max_date' => 'nullable|date',
            'half' => 'nullable|boolean',
            'class_name' => 'nullable|string',
            'helper' => 'nullable|string',
            'helper_en' => 'nullable|string',
            'title' => 'nullable|string',
            'title_en' => 'nullable|string',
            'min_value' => 'nullable|numeric',
            'max_value' => 'nullable|numeric',
            'show_checkbox' => 'nullable|boolean',
            'show_periodique' => 'nullable|boolean',
        ]);

        $question->update($formFields);

        return response()->json($question);
    }
}
