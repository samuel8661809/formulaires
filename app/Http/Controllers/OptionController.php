<?php

namespace App\Http\Controllers;

use App\Models\Formulaire;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    // Show all Question's options
    public function index(Formulaire $formulaire, Question $question) {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }
        $options = $question->options;
        return response()->json($options);
    }

    // Show Single Option
    public function show(Formulaire $formulaire, Question $question, Option $option)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        if ($option->question_id !== $question->id) {
            return response()->json(['error' => 'Option not found for this question'], 404);
        }

        return response()->json($option);
    }

    // Store Option
    public function store(Request $request, Formulaire $formulaire, Question $question)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        $formFields = $request->validate([
            'option_value' => 'nullable|string',
            'option_value_en' => 'nullable|string',
        ]);

        $option = $question->options()->create($formFields);

        return response()->json($option, 201);
    }

    // Update Option
    public function update(Request $request, Formulaire $formulaire, Question $question, Option $option)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        if ($option->question_id !== $question->id) {
            return response()->json(['error' => 'Option not found for this question'], 404);
        }

        $formFields = $request->validate([
            'option_value' => 'nullable|string',
            'option_value_en' => 'nullable|string',
        ]);

        $option->update($formFields);

        return response()->json($option);
    }

    // Delete Option
    public function destroy(Formulaire $formulaire, Question $question, Option $option)
    {
        if ($question->formulaire_id !== $formulaire->id) {
            return response()->json(['error' => 'Question not found for this formulaire'], 404);
        }

        if ($option->question_id !== $question->id) {
            return response()->json(['error' => 'Option not found for this question'], 404);
        }

        $option->delete();

        return response()->json(['message' => 'Option deleted successfully']);
    }
}
