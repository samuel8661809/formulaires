<?php

namespace App\Http\Controllers;

use App\Models\Formulaire;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FormulaireController extends Controller
{
    // Show all Formulaires
    public function index() {
        $formulaires = Formulaire::all();
        return response()->json($formulaires, Response::HTTP_OK);
    }

    // Show Single Formulaire
    public function show(Formulaire $formulaire) {
        return response()->json($formulaire, Response::HTTP_OK);
    }

    // Store Formulaire Data
    public function store(Request $request) {
        $formFields = $request->validate([
            'form_name' => 'required',
        ]);

        $formFields['user_id'] = auth()->id();

        $formulaire = Formulaire::create($formFields);
        return response()->json($formulaire, Response::HTTP_CREATED);
    }

    // Delete Formulaire
    public function destroy(Formulaire $formulaire) {
        $formulaire->delete();
        return response()->json(['message' => 'Post deleted'], Response::HTTP_OK);
    }

    // Update Formulaire
    public function update(Request $request, Formulaire $formulaire) {
        $formFields = $request->validate([
            'form_name' => 'required',
        ]);

        $formFields['user_id'] = auth()->id();

        $formulaire->update($formFields);
        return response()->json($formulaire, Response::HTTP_OK);
    }
}
