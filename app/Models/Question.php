<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($question) {
            $lastFieldNumber = self::where('formulaire_id', $question->formulaire_id)->max('field_number');
            $question->field_number = $lastFieldNumber + 1;
        });
    }

    public function formulaire() {
        return $this->belongsTo(Formulaire::class);
    }

    public function options() {
        return $this->hasMany(Option::class);
    }
}
